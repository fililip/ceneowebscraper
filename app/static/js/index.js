const scrapeButton = document.querySelector("#scrape-button");
const copyButton = document.querySelector("#copy-button");
const progressBar = document.querySelector("#scrape-progress-bar");
const productInput = document.querySelector("#product-id");
const error = document.querySelector("#error");
const fileView = document.querySelector("#file-view");
const responseView = document.querySelector("#response");
const canvasContainer = document.querySelector(".canvas-container");
let canvas = null;

let progressInterval = 0;
let progress = 0.0;

let productID = "";
let getHandler = null;
let response = {};

function clamp(num, min, max) {
    return Math.min(Math.max(num, min), max);
}

function canvasSetup(recommendedCount, notRecommendedCount) {
    canvasContainer.innerHTML = "";
    let c = document.createElement("canvas");
    canvas = c;
    canvasContainer.appendChild(c);

    new Chart(canvas, {
        type: "pie",
        data: {
            labels: [
                'Recommended',
                'Not recommended'
            ],
            datasets: [{
                label: "Recommendation distribution",
                data: [
                    recommendedCount,
                    notRecommendedCount
                ],
                backgroundColor: [
                    'rgb(99, 255, 122)',
                    'rgb(255, 122, 99)'
                ],
                hoverOffset: 4
            }]
        },
    });
}

function setProgress(barContainer, percentage) {
    percentage = clamp(percentage, 0.0, 1.0);
    percentage = `${Math.round(percentage * 100.0)}%`;

    let bar = barContainer.querySelector(".progress-bar-progress");
    bar.style.width = percentage;

    let text = barContainer.querySelector(".progress-bar-text");
    text.innerText = percentage;
}

function updateBar(progressBar) {
    progress += 0.0001 * (0.99 - progress);
    setProgress(progressBar, progress);
}

function responseHandler(res) {
    clearInterval(progressInterval);
    productInput.disabled = null;
    setProgress(progressBar, 1.0);
    response = res;
    if ("error" in res) {
        error.style.display = "block";
        error.innerText = res.error;
    } else {
        responseView.style.display = "block";
    }
    fileView.querySelector("textarea").innerHTML =
        JSON.stringify(res, null, 4);
    
    let recommendedCount = 0,
        notRecommendedCount = 0;

    for (let i = 0; i < res.length; i++) {
        const review = res[i];
        
        if (review.recommendation != null) {
            let r = !!review.recommendation;

            recommendedCount += r ? 1 : 0;
            notRecommendedCount += !r ? 1 : 0;
        }
    }

    canvasSetup(recommendedCount, notRecommendedCount);

    getHandler = null;
}

function errorHandler() {
    error.style.display = "block";
    error.innerText = "Invalid product ID";
}

scrapeButton.addEventListener("click", () => {
    if (getHandler != null) return;

    try {
        productInput.disabled = null;
        responseView.style.display = null;
        error.style.display = null;
        error.innerText = "";
        response = {};
        progress = 0.0;
        setProgress(progressBar, 0.0);
        productInput.disabled = 1;
        productID = parseInt(productInput.value);
        if (isNaN(productID)) throw "";
        productID = productID.toString();
        getHandler = fetch(`/scrape/${productID}`)
            .then(res => { return res.json(); })
            .catch(errorHandler)
            .then(responseHandler)
            .catch(errorHandler);
        clearInterval(progressInterval);
        progressInterval = setInterval(() => updateBar(progressBar), 10);
    } catch (e) {
        console.log(e);
        error.style.display = "block";
        error.innerText = "Invalid product ID";
        productInput.disabled = null;
    }
});

copyButton.addEventListener("click", () => {
    navigator.clipboard.writeText(JSON.stringify(response))
        .then(() => {}, () => {});
});