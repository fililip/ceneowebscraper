from app import app
from flask import render_template
from scraper import scrape

@app.route("/")
@app.route("/index")
def index():
    return render_template("index.jinja", page = "home")

@app.route("/scrape/<id>")
def scraper(id):
    scraped = scrape(id)
    return scraped

@app.route("/about")
def about():
    return render_template("about.jinja", page = "about")